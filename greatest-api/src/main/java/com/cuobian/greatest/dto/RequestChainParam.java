package com.cuobian.greatest.dto;

import com.cuobian.greatest.enums.ChainEnum;
import lombok.Data;

import java.util.List;

@Data
public class RequestChainParam {

    private String targetAddress;

    private List<ChainEnum> chainEnums;
}
