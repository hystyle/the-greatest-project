package com.cuobian.greatest.utils;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.reader.MapSheetReader;

import java.io.FileInputStream;
import java.util.*;

public class ExcelUtil {
    /**
     * easy-poi
     * <p>
     * 功能描述：根据接收的Excel文件来导入多个sheet,根据索引可返回一个集合
     *
     * @param filePath   导入文件路径
     * @param sheetIndex 导入sheet索引
     * @param titleRows  表标题的行数
     * @param headerRows 表头行数
     * @param pojoClass  Excel实体类
     * @return
     */
    public static <T> List<T> importExcel(String filePath, int sheetIndex, Integer titleRows, Integer headerRows, Class<T> pojoClass) {
        // 根据file得到Workbook,主要是要根据这个对象获取,传过来的excel有几个sheet页
        ImportParams params = new ImportParams();
        // 第几个sheet页
        params.setStartSheetIndex(sheetIndex);
        params.setTitleRows(titleRows);
        params.setHeadRows(headerRows);
        List<T> list = null;
        try {
            list = ExcelImportUtil.importExcel(new FileInputStream(filePath), pojoClass, params);
        } catch (NoSuchElementException e) {
            throw new RuntimeException("模板不能为空");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * hutool
     *
     * @param filePath   导入文件路径
     * @param sheetIndex 导入sheet索引
     * @return
     */
    public static List<Map<String, Object>> readerExcelInfo(String filePath, Integer sheetIndex) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (filePath.isEmpty()) {
            return Collections.emptyList();
        }
        try {

            ExcelReader excelReader = cn.hutool.poi.excel.ExcelUtil.getReader(new FileInputStream(filePath), sheetIndex);
            //读取为Map列表，默认第一行为标题行，Map中的key为标题，value为标题对应的单元格值。
            result = excelReader.readAll();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param filePath       导入文件路径
     * @param sheetName      导入sheet名称
     * @param headerRowIndex 表头行数
     * @param startRowIndex  起始行
     * @return
     */
    public static List<Map<String, Object>> readerExcelInfo(String filePath, String sheetName,
                                                            Integer headerRowIndex, Integer startRowIndex) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (filePath.isEmpty()) {
            return Collections.emptyList();
        }
        try {

            ExcelReader excelReader = cn.hutool.poi.excel.ExcelUtil.getReader(new FileInputStream(filePath), sheetName);
            //读取为Map列表，默认第一行为标题行，Map中的key为标题，value为标题对应的单元格值。
            final MapSheetReader reader = new MapSheetReader(headerRowIndex, startRowIndex, Integer.MAX_VALUE);
            return excelReader.read(reader);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
