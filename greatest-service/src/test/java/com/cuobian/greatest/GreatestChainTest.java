package com.cuobian.greatest;

import com.cuobian.greatest.domain.chain.HandlerChain;
import com.cuobian.greatest.dto.RequestChainParam;
import com.cuobian.greatest.enums.ChainEnum;
import com.sun.tools.javac.util.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

public class GreatestChainTest extends BaseTest{

    @Autowired
    HandlerChain handlerChain;

    @Test
    public void Test() {
        RequestChainParam param = new RequestChainParam();
        param.setChainEnums(Arrays.asList(ChainEnum.SELECTION, ChainEnum.PUBLISH));

        Assert.check(handlerChain.exec("t1", param));
    }
}
