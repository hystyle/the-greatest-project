package com.cuobian.greatest;


import cn.hutool.core.map.BiMap;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.cuobian.greatest.utils.ExcelUtil;
import com.cuobian.greatest.utils.ListUtil;
import org.junit.jupiter.api.Test;

import java.util.*;

public class ExcelTest extends BaseTest {

    private static final String path = "/Users/huyi/Downloads/tagmap.xlsx";

    /**
     * @param categoryName 原目录名称
     * @return 一级目录
     */
    public static String getFirstCategory(String categoryName, List<BiMap<String, Object>> firstCategories) {
        for (BiMap<String, Object> category : firstCategories) {
            String categoryKey = category.getKey(categoryName);
            if (StrUtil.isNotBlank(categoryKey)) {
                return categoryKey;
            }
        }
        return null;
    }

    /**
     * @param firstCategory 一级目录
     * @param categoryName  原目录名称
     * @return 二级目录
     */
    public static String getSecondCategory(String firstCategory, String categoryName, Map<String, List<BiMap<String, Object>>> secondCategoryRelation) {
        List<BiMap<String, Object>> secondCategories = secondCategoryRelation.get(firstCategory);
        for (BiMap<String, Object> secondCategory : secondCategories) {
            String categoryKey = secondCategory.getKey(categoryName);
            if (StrUtil.isNotBlank(categoryKey)) {
                return categoryKey;
            }
        }

        return null;

    }

    /**
     * 获取一级目录对应关系
     *
     * @return
     */
    public static List<BiMap<String, Object>> getFirstCategoryRelation() {
        List<Map<String, Object>> categoryList = ExcelUtil.readerExcelInfo(path, 1);
        List<BiMap<String, Object>> category = ListUtil.convertBiMap(categoryList);
        return category;
    }


    /**
     * 获取二级目录对应关系
     *
     * @param categoryNames 原目录集合
     * @return 一级目录->二级目录对应关系集合
     */
    public static Map<String, List<BiMap<String, Object>>> getSecondCategoryRelation(Set<String> categoryNames) {
        Map<String, List<BiMap<String, Object>>> result = new HashMap<>();

        // 获取一级类目对应的二级类目
        Iterator<String> iterator = categoryNames.iterator();
        while (iterator.hasNext()) {
            String categoryName = iterator.next();
            List<Map<String, Object>> categoryList = ExcelUtil.readerExcelInfo(path, categoryName, 1, 2);
            List<BiMap<String, Object>> category = ListUtil.convertBiMap(categoryList);
            result.put(categoryName, category);
        }

        return result;
    }

    @Test
    public void logic() {
        List<BiMap<String, Object>> firstCategories = getFirstCategoryRelation();
        // 获取所有一级类目
        Set<String> firstCategoryNames = firstCategories.get(0).keySet();
        Map<String, List<BiMap<String, Object>>> secondCategoryRelation = getSecondCategoryRelation(firstCategoryNames);

        // 从数据量捞数据
        String title = "Skechers Men's Energy Afterburn Lace-Up Sneaker";
        String category = "Clothing, Shoes & Jewelry › Men › Shoes › Athletic › Running › Road Running";
        String brand = "Skechers";
        String[] split = category.split("›");

        String f1 = split[0].trim();
        String subclass = split[split.length - 1].trim();

        System.err.println("f1:" + f1.trim() + ",   " + "subclass:" + subclass);

        String firstCategory = getFirstCategory(f1, firstCategories);
        if (StrUtil.isNotBlank(firstCategory)) {
            String secondCategory = getSecondCategory(firstCategory, subclass, secondCategoryRelation);

            // 标签 = 一级类目 + 子类目
            List<String> tags = new ArrayList<>();
            tags.add(firstCategory);
            tags.add(secondCategory);
            System.err.println("tags:" + JSON.toJSONString(tags));

            // 标题 = 品牌 子类目 - 标题
            String newTitle = brand + " " + subclass + " - " + title;
            System.err.println("newTitle: " + newTitle);
        }


    }


}
