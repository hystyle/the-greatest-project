package com.cuobian.greatest.service.impl;

import com.cuobian.greatest.domain.chain.AbstractHandler;
import com.cuobian.greatest.dto.RequestChainParam;
import com.cuobian.greatest.enums.ChainEnum;
import com.cuobian.greatest.interfaces.IScreenerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service("screenerService")
public class ScreenerServiceImpl extends AbstractHandler implements IScreenerService {

    private final ChainEnum chain = ChainEnum.SCREENER;

    @Override
    public void screener() {
        log.warn("init screener...");
    }

    @Override
    public Boolean process(RequestChainParam param) {
        if (param.getChainEnums().contains(chain)) {
            screener();
        }
        return super.next(param);
    }
}
