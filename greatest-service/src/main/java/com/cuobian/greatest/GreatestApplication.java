package com.cuobian.greatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.cuobian.greatest.*")
public class GreatestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreatestApplication.class, args);
	}

}
