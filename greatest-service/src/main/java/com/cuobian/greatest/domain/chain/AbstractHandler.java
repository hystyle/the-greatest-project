package com.cuobian.greatest.domain.chain;

import com.cuobian.greatest.dto.RequestChainParam;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

public abstract class AbstractHandler implements Handler {

    @Getter
    @Setter
    private AbstractHandler nextHandler;

    protected Boolean next(RequestChainParam param) {

        if (Objects.isNull(nextHandler)) {
            return true;
        }

        return nextHandler.process(param);
    }

    public abstract Boolean process(RequestChainParam param);
}
