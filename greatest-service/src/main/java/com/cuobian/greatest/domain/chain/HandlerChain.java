package com.cuobian.greatest.domain.chain;

import cn.hutool.core.util.StrUtil;
import com.cuobian.greatest.dto.RequestChainParam;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * chain-compose-strategy
 */
@Component
public class HandlerChain {

    public final Map<String, AbstractHandler> abstractHandlers = new HashMap<>();

    /**
     * 初始化流程 模型 <K, V></>
     *
     * @param key      流程key
     * @param handlers 流程服务集
     */
    public void initChain(String key, AbstractHandler... handlers) {
        List<AbstractHandler> abstractHandlers = Arrays.asList(handlers);
        for (int i = 0; i < abstractHandlers.size(); i++) {
            if (i != abstractHandlers.size() - 1) {
                abstractHandlers.get(i).setNextHandler(abstractHandlers.get(i + 1));
            }
        }
        this.abstractHandlers.put(key, abstractHandlers.get(0));
    }

    public boolean exec(String key, RequestChainParam param) {
        if (StrUtil.isNotBlank(key)) {
            AbstractHandler abstractHandler = this.abstractHandlers.get(key);
            if (!abstractHandler.process(param)) {
                System.err.println("error");
                return false;
            }
            return true;
        }
        return false;
    }
}
