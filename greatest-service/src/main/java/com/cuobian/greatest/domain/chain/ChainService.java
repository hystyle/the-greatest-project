package com.cuobian.greatest.domain.chain;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 初始化流程服务
 */
@Component
public class ChainService {

    @Autowired
    private HandlerChain handlerChain;

    @Autowired
    private AbstractHandler screenerService;

    @PostConstruct
    public void addHandler() {
        handlerChain.initChain("t1", screenerService);
    }

}
